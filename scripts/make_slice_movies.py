################################################################################
# Makes x,y,z movies (decks of .pngs) of a multi-panel figure of several fields 
# from a single simulation.
# 
# Arguments
#   sim_dir : Directory of the simulation
#   title   : (Optional) Title to put in each figure of the movie. Otherwise 
#             uses the leaf directory name as a title
################################################################################

import sys
from pathlib import Path

if len(sys.argv) < 2:
    print("""Insufficient arguments. Use case:
                sim_dir : Directory of the simulation
                title   : (Optional) Title to put in each figure of the movie. Otherwise 
                          uses the leaf directory name as a title
          """)
    raise Exception("Invalid commandline arguments")

sim_dir = sys.argv[1]

if len(sys.argv) >= 3:
    sim_title = sys.argv[2]
else:
    sim_title = sim_dir.split("/")[-1]

fig_dir = f"{sim_dir}/figures"
Path(fig_dir).mkdir(parents=True, exist_ok=True)

print(f"Reading from {sim_dir} with title {sim_title} into {fig_dir}")


import os #os.makedirs
import glob 
import numpy as np
import re

import hashlib
import json

# IMPORTANT: MAKE SURE TO USE THE `parthenon-frontend` BRANCH FROM 
# https://github.com/forrestglines/yt/tree/parthenon-frontend

import yt
yt.set_log_level(50)
yt.enable_parallelism()

from yt import derived_field
@derived_field(name="ACCEPT_entropy",units="keV*cm**2",sampling_type="cell")
def __accept_entropy(field,data):
    mw = data.ds.mu
    K = data["kT"]/( data["density"]/(mw*yt.units.mh))**(2./3.)
    return K.in_units("keV*cm**2")

import matplotlib as mpl
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import ImageGrid
import numpy as np

# Make figure backgrounds white instead of transparent
mpl.rcParams["figure.facecolor"]="white"

# Glob what filenames to read
outputs_glob = f"{sim_dir}/parthenon.restart.*.rhdf"
output_filenames = glob.glob(outputs_glob)
output_filenames.sort()

#Define a cooling table for yt to read from
cooling_table_filename="/u/glines/code/magnetized-clusters/inputs/cooling_tables/schure.cooling"


# ## Define and set specifications for slice data and slice plots
# 
# Defines a slice of data for yt, including size, location, resolution, orientation, and what fields to save to disk

class SliceDataSpec:
    """
    SliceDataSpec
    
    Defines a slice of data for yt, including size, location, 
    resolution, orientation, and what fields to save to disk
    """
    hash_digits = 8
    def __init__(self,
                 fields, #Fields to save to disk
                 axis,
                 coord,
                 width,
                 resolution,
                 center=(0,0,0),
                 ):
        self.fields = fields
        self.axis = axis
        self.coord = coord
        self.width = width
        self.resolution = resolution
        self.center = center
        
        #Gives yz, xz, xy plots depending on the axis
        #FIXME: Double check how yt reads slices and in what order the axes will be
        if(axis=="x"):
            self.axis_1_idx = 1
            self.axis_2_idx = 2
        elif(axis=="y"):
            self.axis_1_idx = 2
            self.axis_2_idx = 0
        elif(axis=="z"):
            self.axis_1_idx = 0
            self.axis_2_idx = 1
        else:
            raise Exception(f"Unknown axis {axis}")

        self.axis_1_name = ["x","y","z"][self.axis_1_idx]
        self.axis_2_name = ["x","y","z"][self.axis_2_idx]
    def __str__(self):
        return (f"fields={self.fields}\n"
                f"axis={self.axis}\n"
                f"coord={self.coord}\n"
                f"center={self.center}\n"
                f"width={self.width}\n"
                f"resolution={self.resolution}")

    def __eq__(self, other):
       
        # Equality Comparison between two objects
        return np.all([self.fields == other.fields,
                       self.axis == other.axis,
                       self.coord == other.coord,
                       self.width == other.width,
                       self.resolution == other.resolution,
                       self.center == other.center])
 
    
    def __hash__(self):
        """
        Hash function for a slice data object. The hash is used to determine
        if the defined slice as already been created
        """
       
        # hash(custom_object)
        params = (self.fields,self.axis,self.coord,
                  self.width,self.resolution,self.center)
        my_hash = int(hashlib.sha224(json.dumps(params).encode(),).hexdigest(),16) % int(10**self.hash_digits)
        return my_hash
    
    def timeseries_filename(self):
        return f"slice.ts.{hash(self)}.h5"
    def specs_filename(self):
        return f"slice.specs.{hash(self)}.txt"
    
    def save_slice_ts(self,slice_data,slice_maxs,slice_mins,data_dir):
        
        Path(data_dir).mkdir(parents=True, exist_ok=True)
        
        data = {f"{key}.data":value for (key,value) in slice_data.items()} \
              |{f"{key}.maxs":value for (key,value) in slice_maxs.items()} \
              |{f"{key}.mins":value for (key,value) in slice_mins.items()}
        fake_ds = {"current_time": yt.YTQuantity(0, "Myr")}
        yt.save_as_dataset(fake_ds,f"{fig_dir}/{self.timeseries_filename()}",data)
        
        with open(f"{fig_dir}/{self.specs_filename()}","w") as f:
            f.write(str(self))
            
        
    def slice_ts_len(self,data_dir):
        
        if not os.path.isfile(f"{data_dir}/{self.timeseries_filename()}"):
            return 0
        
        ds = yt.load(f"{data_dir}/{self.timeseries_filename()}")
        
        return len( ds.data[f"{self.fields[0]}.maxs"])
        
    def load_slice_ts(self,data_dir):
        if not os.path.isfile(f"{data_dir}/{self.timeseries_filename()}"):
            return ( {field:np.empty((0,self.resolution,self.resolution))
                      for field in self.fields},
                     {field:np.empty((0,))
                      for field in self.fields},
                     {field:np.empty((0,))
                      for field in self.fields})
        
        ds = yt.load(f"{data_dir}/{self.timeseries_filename()}")
        
        slice_data = {field:ds.data[f"{field}.data"] for field in self.fields}
        slice_maxs = {field:ds.data[f"{field}.maxs"] for field in self.fields}
        slice_mins = {field:ds.data[f"{field}.mins"] for field in self.fields}
        
        return slice_data,slice_maxs,slice_mins
    
    def get_positions(self):
        
        width = yt.YTQuantity(*self.width)
        center = yt.YTArray(self.center,"kpc")
        axis_1 = np.linspace(center[self.axis_1_idx]-width/2.,
                             center[self.axis_1_idx]+width/2.,
                             self.resolution)
        axis_2 = np.linspace(center[self.axis_2_idx]-width/2.,
                             center[self.axis_2_idx]+width/2.,
                             self.resolution)
        
        return np.meshgrid(axis_1,axis_2)
        


#Define a list of slices for each orientation, xyz
slice_data_specs = [SliceDataSpec(
    fields=("density","pressure","temperature","cooling_time","cooling_rate",
        "mach_number", "velocity_x","velocity_y","velocity_z",
        "magnetic_field_x","magnetic_field_y","magnetic_field_z",
        "magnetic_energy_density","ACCEPT_entropy"),
    axis = axis,
    coord = 0,
    width = (40,"kpc"),
    resolution=512,
            ) for axis in ["x","y","z"]]

print([f"{str(hash(sds)).zfill(8)}" for sds in slice_data_specs]) 

# ## Read Slice Data (using parallel yt)
# 
# Reads the slice data, creates a fixed-resolution-buffer from that slice, and computes the min and max for each field in the slice.  Theses mins/maxes will be useful for make plots/movies with generated axes that are fixed across the movie.

#More convineint dictionaries of slice data, maxs, and mins
#How to index each dictionary:
slice_datas = {}#slice_datas[hash(sds)][field][output_idx]
slice_maxs = {}#slice_maxs[hash(sds)][field][output_idx]
slice_mins = {}#slice_mins[hash(sds)][field][output_idx]

len_saved = {}#len_saved[hash(sds)]
#Load Existing data
for sds in slice_data_specs:
    sds_slice_datas,sds_slice_maxs,sds_slice_mins = sds.load_slice_ts(fig_dir)
    slice_datas[hash(sds)] = sds_slice_datas
    slice_maxs[hash(sds)] = sds_slice_maxs
    slice_mins[hash(sds)] = sds_slice_mins
    len_saved[hash(sds)] = sds.slice_ts_len(fig_dir)


#Create a time series of full simulation data
ts = yt.DatasetSeries(output_filenames,parameters={
    "cooling_table_filename":cooling_table_filename,
    "cooling_table_log_temp_col":0,
    "cooling_table_log_lambda_col":1,
    "cooling_table_lambda_units_cgs":1})

ds = ts[0]
ds.index

data_missing = False
len_saved = { hash(sds):sds.slice_ts_len(fig_dir) for sds in slice_data_specs}

#Check if the slice data already exists and is long enough
for sds in slice_data_specs:
    if not len(slice_datas[hash(sds)][sds.fields[0]]) == len(ts):
        data_missing = True
        missing_len = len(ts) - len(slice_datas[hash(sds)][sds.fields[0]])
        # Resize arrays to accomotate new data
        slice_datas[hash(sds)] = { 
            field:ds.arr(np.append( slice_datas[hash(sds)][field],
                            np.empty((missing_len,sds.resolution,sds.resolution)),axis=0),
                            ds.field_info["gas",field].units)
            for field in sds.fields}
        slice_maxs[hash(sds)] = { 
            field:ds.arr(np.append( slice_maxs[hash(sds)][field],
                            np.empty((missing_len)),axis=0),
                            ds.field_info["gas",field].units)
            for field in sds.fields}
        slice_mins[hash(sds)] = { 
            field:ds.arr(np.append(slice_mins[hash(sds)][field],
                            np.empty((missing_len)),axis=0),
                            ds.field_info["gas",field].units)
            for field in sds.fields}
        
if data_missing:
    print("Need to read data")
    #Go through all datasets in time series (forget yt parallel programming)
    for ds_idx,ds in enumerate(ts):
        
        #Add fields to dataset
        def _cold_mass_temp_x(field, data,temp_thresh):
            temp = data['temperature']
            mass = data['density'] * data['cell_volume']
            ind = np.where(temp > temp_thresh)
            mass[ind] = 0.0
            return mass


        cold_mass_temp_threshs = (1e4,1e5,4e7,np.inf)
        cold_mass_full_names = [ ("gas", f"cold_mass_temp_{temp_thresh:.1e}") 
                                for temp_thresh in cold_mass_temp_threshs ]


        for temp_thresh,full_name in zip(cold_mass_temp_threshs,cold_mass_full_names):
            function=lambda field,data,temp_thresh=temp_thresh:                 _cold_mass_temp_x(field,data,temp_thresh)
            function.__name__ = full_name[1]

            ds.add_field(full_name, 
                function= function,
                sampling_type="local",
                units='Msun')
        
        for sds in slice_data_specs:
            if ds_idx >= len_saved[hash(sds)]:
                print(f"Reading slice {hash(sds)} from AthenaPK file {ds}")
                sys.stdout.flush()

                slc = ds.slice(axis=sds.axis,coord=sds.coord,center=sds.center)
                frb = slc.to_frb(width=sds.width,resolution=sds.resolution)

                for field in sds.fields:
                    slice_datas[hash(sds)][field][ds_idx] = frb[field].copy()
                    slice_maxs[hash(sds)][field][ds_idx] = frb[field].max()
                    slice_mins[hash(sds)][field][ds_idx] = frb[field].min()

                #It's unnecessary but easier to store 
                #for axis in (sds.axis_1,sds.axis_2):
                #    field_datas[hash(sds)][axis][ds_idx] = frb[axis].copy() 
    for sds in slice_data_specs:
        #Save the data
        sds.save_slice_ts(slice_datas[hash(sds)],slice_maxs[hash(sds)],slice_mins[hash(sds)],fig_dir)


# ## Define slice plot parameters
# 
# Define how to plot each field from the slices we've read. This includes what kind of color-axes to use (i.e. linear, log, symlog), colormap, whether to include quiver/vector arrows with two other fields, and a label.

class SlicePlotSpec:
 """
 SliceDataSpec
 
 Define how to plot each field from the slices we've read. 
 This includes what kind of color-axes to use (i.e. linear, log, symlog), 
 colormap, whether to include quiver/vector arrows with two other fields, 
 and a label.
 """
 def __init__(self,
             slice_data_spec,
             field,
             units,
             norm_type = "linear",
             norm_params = None,
             norm_sym = False,
             norm = None,
             cmap = "viridis",
             quiver_fields=None,
             quiver_params=None,
             field_label=None):
     self.slice_data_spec = slice_data_spec
     self.field = field
     self.units = units
     self.norm_type = norm_type
     if norm_params is None:
         norm_params = {}
     self.norm_params = norm_params
     self.norm_sym = norm_sym
     self.norm = norm
     
     self.quiver_fields = quiver_fields
     self.quiver_params = quiver_params
     
     if norm_type == "log" and norm_sym == True:
         raise Exception('Logarithmic norm (norm_type="log") and symmetric norm_sym (norm_sym=True) are incompatible')
         
     if field_label is None:
         field_label = field
         
     self.field_label = field_label
     

# Create a dictionary for how to plot each field

#How to index the dictionary:
#slice_plot_specs[hash(sds)][field]

slice_plot_specs = { hash(sds): { sps.field:sps for sps in [
     SlicePlotSpec(sds,"density","g/cm**3","log",cmap="viridis",field_label="$\\rho$"),#norm=mpl.colors.LogNorm(yt.YTQuantity(1e-24,1e-26,"g/cm**3"))),
     SlicePlotSpec(sds,"pressure","dyne/cm**2","log",cmap="cividis",field_label="$P$"),
     SlicePlotSpec(sds,"temperature","K","log",field_label="$T$"),
     SlicePlotSpec(sds,"cooling_time","Myr","log",cmap="plasma",field_label="$t_{cool}$"),
     SlicePlotSpec(sds,"cooling_rate","erg/s/cm**3","log",cmap="magma",field_label="$\\Lambda_{cool}$"),
     SlicePlotSpec(sds,"velocity_x","cm/s","symlog",norm_sym=True,cmap="PuOr",field_label="$v_x$"),
     SlicePlotSpec(sds,"velocity_y","cm/s","symlog",norm_sym=True,cmap="PuOr",field_label="$v_y$"),
     SlicePlotSpec(sds,"velocity_z","cm/s","symlog",norm_sym=True,cmap="PuOr",field_label="$v_z$"),
     SlicePlotSpec(sds,"mach_number","","log",field_label="$M_s$"),
     SlicePlotSpec(sds,"magnetic_field_x","Gauss","symlog",norm_sym=True,cmap="PiYG",field_label="$B_x$"),
     SlicePlotSpec(sds,"magnetic_field_y","Gauss","symlog",norm_sym=True,cmap="PiYG",field_label="$B_y$"),
     SlicePlotSpec(sds,"magnetic_field_z","Gauss","symlog",norm_sym=True,cmap="PiYG",field_label="$B_z$"),
     SlicePlotSpec(sds,"magnetic_energy_density","erg/cm**3","log",field_label="$E_B$"),
     SlicePlotSpec(sds,"ACCEPT_entropy","keV*cm**2","log",field_label="$K$"),
 ]} for sds in slice_data_specs }


# ## Set up the bounds for the color-axes
# 
# Find `vmin` and `vmax` for each plotted field in each slice from `slice_mins` and `slice_maxs` depending on the color-axis type for each field in each slice

# First, finish setting up the coloring norms
for sds in slice_data_specs:
    for field,sps in slice_plot_specs[hash(sds)].items():

        #Only setup the norm if it's not yet specified
        if sps.norm is None:
            if "vmax" not in sps.norm_params or sps.norm_params["vmax"] is None:
                sps.norm_params["vmax"] = slice_maxs[hash(sds)][field].max().in_units(sps.units)
            if "vmin" not in sps.norm_params or sps.norm_params["vmin"] is None:
                sps.norm_params["vmin"] = slice_mins[hash(sds)][field].min().in_units(sps.units)

            if sps.norm_sym:
                vmax = np.abs(yt.YTArray([sps.norm_params["vmin"],sps.norm_params["vmax"]])).max()

                sps.norm_params["vmax"] =  vmax
                sps.norm_params["vmin"] = -vmax

            if sps.norm_type == "linear":
                sps.norm = mpl.colors.Normalize(**sps.norm_params)

            if sps.norm_type == "log":
                if sps.norm_params["vmin"] <= 0:
                    sps.norm_params["vmin"] = 1e-10*sps.norm_params["vmax"]
                sps.norm = mpl.colors.LogNorm(**sps.norm_params)

            elif sps.norm_type == "symlog":
                if "linthresh" not in sps.norm_params or sps.norm_params["linthresh"] is None:
                    sps.norm_params["linthresh"] = 1e-4*sps.norm_params["vmax"]
                sps.norm = mpl.colors.SymLogNorm(**sps.norm_params)

        #Since everything is by reference, is this last line necessary?
        #slice_plot_specs[i] = sps
        slice_plot_specs[hash(sds)][field] = sps


# ## Define figures
# 
# Define the figures to be plotted and saved, including plots to make, number of rows and columns, figure size, title if any, and prefix to the figure filename

class SliceFigureSpec:
    """
    SliceFigureSpec
    
    Define the figures to be plotted and saved,
    including plots to make, number of rows and
    columns, figure size, title if any, and prefix
    to the figure filename
    """
    def __init__(self,
                slice_plot_specs,
                nrows,
                ncols,
                save_prefix,
                title=None,
                figsize=None,
                ):
        #self.slice_data_specs[field]
        self.slice_plot_specs = slice_plot_specs
        self.nrows = nrows
        self.ncols = ncols
        self.save_prefix = save_prefix
        self.title = title
        if figsize is None:
            figsize = (0.5 + 2.5*ncols,0.5 + 2*nrows)
        self.figsize = figsize
        #self.slice_plot_specs[]
        
# Make a list of figures to make movies of

slice_figure_specs = [
    #Here starts figures for each slice orientation
    SliceFigureSpec(
        slice_plot_specs= [
            slice_plot_specs[hash(sds)][field] 
            for field in [
                "density","temperature",
                "cooling_time","ACCEPT_entropy",
                "mach_number","magnetic_energy_density"]],
        nrows=3,
        ncols=2,
        save_prefix=f"slice_{sds.axis}",
        title=f"{sim_title} {sds.axis}-Slice"
        )
    for sds in slice_data_specs ]
"""
+ [
    #Here stars figures for density only figures for each slice orientation
    SliceFigureSpec(
        slice_plot_specs= [
            slice_plot_specs[hash(sds)][field] 
            for field in [
                "density",]],
        nrows=1,
        ncols=1,
        save_prefix=f"density_{sds.axis}",
        title=f"{sim_title} {sds.axis}-Slice"
        )
    for sds in slice_data_specs ]
    """;

output_idxs_to_plot = range(len(ts))
#output_idxs_to_plot = range(0,300)

os.makedirs(fig_dir, exist_ok=True)  #Make sure figure directory exists


# ## Plot figures for each output
# 
# Plots the figures defined above into pngs. You can collate these pngs together into a `.mov` with FFmpeg, such as with
# 
# ```
# module load FFmpeg
# ffmpeg -framerate 10 -pattern_type glob -i "slice_x*.png" -f mp4 -vcodec h264 -pix_fmt yuv420p -b:v 8M -r 10 slice_x.mov -y && ffmpeg -framerate 10 -pattern_type glob -i "slice_y*.png" -f mp4 -vcodec h264 -pix_fmt yuv420p -b:v 8M -r 10 slice_y.mov -y  && ffmpeg -framerate 10 -pattern_type glob -i "slice_z*.png" -f mp4 -vcodec h264 -pix_fmt yuv420p -b:v 8M -r 10 slice_z.mov -y
# ```
# 
# or into a `.gif` with Imagemagick `convert` using
# 
# ```
# convert -delay 20 -loop 0 slice_x_*.png slice_x.gif && convert -delay 20 -loop 0 slice_y_*.png slice_y.gif  && convert -delay 20 -loop 0 slice_z_*.png slice_z.gif
# ```

for output_idx in output_idxs_to_plot:
    if( output_idx >= len(ts) ):
        print(f"Skipping {output_idx}, data doesn't exist")
        continue
    for sfs in slice_figure_specs:

        #FIXME Is sharexy="all" correct? 
        # For now, only if all plots on a figure share the same slice resolution
        fig,axes= plt.subplots(nrows=sfs.nrows,ncols=sfs.ncols,
                               sharex="all",sharey="all",
                               squeeze=False,
                               figsize=sfs.figsize)

        for ax,sps in zip(axes.flatten(),sfs.slice_plot_specs):
            
            #Grab slice_data_spec
            sds = sps.slice_data_spec
            
            #Grab the slice_data
            slice_data = slice_datas[hash(sds)][sps.field][output_idx].in_units(sps.units)
            
            #Grab the coordinate data #HACK assuming kpc axes
            X,Y = sds.get_positions()

            pcm = ax.pcolormesh(X.in_units("kpc"),Y.in_units("kpc"),slice_data,norm=sps.norm)

            fig.colorbar(pcm, ax=ax,label=f"{sps.field_label} in {sps.units}")
            
        #HACK assuming shared axes on all figures in the plot
        for i in range(sfs.nrows):
            axes[i,0].set_ylabel(f"{sds.axis_2_name} [kpc]")
        for j in range(sfs.ncols):
            axes[-1,j].set_xlabel(f"{sds.axis_1_name} [kpc]")

        fig.suptitle(f"{sfs.title} {ts[output_idx].current_time.in_units('Myr'):.1f}" )
        fig.tight_layout()
        plt.savefig(f"{fig_dir}/{sfs.save_prefix}_{output_idx:05d}.png",dpi=300)
        plt.clf()
        plt.close(fig)




