
################################################################################
# Defines and reads x,y,z slices from a single simulation.
# 
################################################################################

import sys
from pathlib import Path


import os #os.makedirs
import glob 
import numpy as np
import re

import hashlib
import json

# IMPORTANT: MAKE SURE TO USE THE `parthenon-frontend` BRANCH FROM 
# https://github.com/forrestglines/yt/tree/parthenon-frontend

import yt
yt.set_log_level(50)
yt.enable_parallelism()

#Define a cooling table for yt to read from
cooling_table_filename="/u/glines/code/magnetized-clusters/inputs/cooling_tables/schure.cooling"

# ## Define and set specifications for slice data and slice plots
# 
# Defines a slice of data for yt, including size, location, resolution, orientation, and what fields to save to disk

class SliceDataSpec:
    """
    SliceDataSpec
    
    Defines a slice of data for yt, including size, location, 
    resolution, orientation, and what fields to save to disk
    """
    hash_digits = 8
    def __init__(self,
                 fields, #Fields to save to disk
                 axis,
                 coord,
                 width,
                 resolution,
                 center=(0,0,0),
                 ):
        self.fields = fields
        self.axis = axis
        self.coord = coord
        self.width = width
        self.resolution = resolution
        self.center = center
        
        #Gives yz, xz, xy plots depending on the axis
        #FIXME: Double check how yt reads slices and in what order the axes will be
        if(axis=="x"):
            self.axis_1_idx = 1
            self.axis_2_idx = 2
        elif(axis=="y"):
            self.axis_1_idx = 2
            self.axis_2_idx = 0
        elif(axis=="z"):
            self.axis_1_idx = 0
            self.axis_2_idx = 1
        else:
            raise Exception(f"Unknown axis {axis}")

        self.axis_1_name = ["x","y","z"][self.axis_1_idx]
        self.axis_2_name = ["x","y","z"][self.axis_2_idx]
    def __str__(self):
        return (f"fields={self.fields}\n"
                f"axis={self.axis}\n"
                f"coord={self.coord}\n"
                f"center={self.center}\n"
                f"width={self.width}\n"
                f"resolution={self.resolution}")

    def __eq__(self, other):
       
        # Equality Comparison between two objects
        return np.all([self.fields == other.fields,
                       self.axis == other.axis,
                       self.coord == other.coord,
                       self.width == other.width,
                       self.resolution == other.resolution,
                       self.center == other.center])
 
    
    def __hash__(self):
        """
        Hash function for a slice data object. The hash is used to determine
        if the defined slice as already been created
        """
       
        # hash(custom_object)
        params = (self.fields,self.axis,self.coord,
                  self.width,self.resolution,self.center)
        my_hash = int(hashlib.sha224(json.dumps(params).encode(),).hexdigest(),16) % int(10**self.hash_digits)
        return my_hash
    
    def timeseries_filename(self):
        return f"slice.ts.{hash(self)}.h5"
    def specs_filename(self):
        return f"slice.specs.{hash(self)}.txt"
    
    def save_slice_ts(self,slice_data,slice_maxs,slice_mins,data_dir):
        
        Path(data_dir).mkdir(parents=True, exist_ok=True)
        
        data = {f"{key}.data":value for (key,value) in slice_data.items()} \
              |{f"{key}.maxs":value for (key,value) in slice_maxs.items()} \
              |{f"{key}.mins":value for (key,value) in slice_mins.items()}
        fake_ds = {"current_time": yt.YTQuantity(0, "Myr")}
        yt.save_as_dataset(fake_ds,f"{data_dir}/{self.timeseries_filename()}",data)
        
        with open(f"{data_dir}/{self.specs_filename()}","w") as f:
            f.write(str(self))
            
        
    def slice_ts_len(self,data_dir):
        
        if not os.path.isfile(f"{data_dir}/{self.timeseries_filename()}"):
            return 0
        
        ds = yt.load(f"{data_dir}/{self.timeseries_filename()}")
        
        return len( ds.data[f"{self.fields[0]}.maxs"])
        
    def load_slice_ts(self,data_dir):
        if not os.path.isfile(f"{data_dir}/{self.timeseries_filename()}"):
            return ( {field:np.empty((0,self.resolution,self.resolution))
                      for field in self.fields},
                     {field:np.empty((0,))
                      for field in self.fields},
                     {field:np.empty((0,))
                      for field in self.fields})
        
        ds = yt.load(f"{data_dir}/{self.timeseries_filename()}")
        
        slice_data = {field:ds.data[f"{field}.data"] for field in self.fields}
        slice_maxs = {field:ds.data[f"{field}.maxs"] for field in self.fields}
        slice_mins = {field:ds.data[f"{field}.mins"] for field in self.fields}
        
        return slice_data,slice_maxs,slice_mins
    
    def get_positions(self):
        
        width = yt.YTQuantity(*self.width)
        center = yt.YTArray(self.center,"kpc")
        axis_1 = np.linspace(center[self.axis_1_idx]-width/2.,
                             center[self.axis_1_idx]+width/2.,
                             self.resolution)
        axis_2 = np.linspace(center[self.axis_2_idx]-width/2.,
                             center[self.axis_2_idx]+width/2.,
                             self.resolution)
        
        return np.meshgrid(axis_1,axis_2)
        


#Define a list of slices for each orientation, xyz
default_slice_data_specs = [SliceDataSpec(
    fields=("density","pressure","temperature","cooling_time","cooling_rate",
        "mach_number", "velocity_x","velocity_y","velocity_z",
        "magnetic_field_x","magnetic_field_y","magnetic_field_z",
        "magnetic_energy_density","ACCEPT_entropy"),
    axis = axis,
    coord = 0,
    width = (40,"kpc"),
    resolution=512,
            ) for axis in ["x","y","z"]]

#print([f"{str(hash(sds)).zfill(8)}" for sds in slice_data_specs]) 

# ## Read Slice Data
# 
# Reads the slice data, creates a fixed-resolution-buffer from that slice, and
# computes the min and max for each field in the slice.  Theses mins/maxes will
# be useful for make plots/movies with generated axes that are fixed across the
# movie.


def get_slice_datas(sim_dir,data_dir,slice_data_specs):
#More convient dictionaries of slice data, maxs, and mins
#How to index each dictionary:
    slice_datas = {}#slice_datas[hash(sds)][field][output_idx]
    slice_maxs = {}#slice_maxs[hash(sds)][field][output_idx]
    slice_mins = {}#slice_mins[hash(sds)][field][output_idx]

    len_saved = {}#len_saved[hash(sds)]


    # Glob what filenames to read
    outputs_glob = f"{sim_dir}/parthenon.restart.*.rhdf"
    output_filenames = glob.glob(outputs_glob)
    output_filenames.sort()

    #Load Existing data
    for sds in slice_data_specs:
        sds_slice_datas,sds_slice_maxs,sds_slice_mins = sds.load_slice_ts(data_dir)
        slice_datas[hash(sds)] = sds_slice_datas
        slice_maxs[hash(sds)] = sds_slice_maxs
        slice_mins[hash(sds)] = sds_slice_mins
        len_saved[hash(sds)] = sds.slice_ts_len(data_dir)


    #Create a time series of full simulation data
    ts = yt.DatasetSeries(output_filenames,parameters={
        "cooling_table_filename":cooling_table_filename,
        "cooling_table_log_temp_col":0,
        "cooling_table_log_lambda_col":1,
        "cooling_table_lambda_units_cgs":1})

    ds = ts[0]
    ds.index

    data_missing = False
    len_saved = { hash(sds):sds.slice_ts_len(data_dir) for sds in slice_data_specs}

    #Check if the slice data already exists and is long enough
    for sds in slice_data_specs:
        if not len(slice_datas[hash(sds)][sds.fields[0]]) == len(ts):
            data_missing = True
            missing_len = len(ts) - len(slice_datas[hash(sds)][sds.fields[0]])

            print(f"Need to read {missing_len} for {str(hash(sds)).zfill(8)} from {sim_dir}")
            # Resize arrays to accomotate new data
            slice_datas[hash(sds)] = { 
                field:ds.arr(np.append( slice_datas[hash(sds)][field],
                                np.empty((missing_len,sds.resolution,sds.resolution)),axis=0),
                                ds.field_info["gas",field].units)
                for field in sds.fields}
            slice_maxs[hash(sds)] = { 
                field:ds.arr(np.append( slice_maxs[hash(sds)][field],
                                np.empty((missing_len)),axis=0),
                                ds.field_info["gas",field].units)
                for field in sds.fields}
            slice_mins[hash(sds)] = { 
                field:ds.arr(np.append(slice_mins[hash(sds)][field],
                                np.empty((missing_len)),axis=0),
                                ds.field_info["gas",field].units)
                for field in sds.fields}
            
    if data_missing:
        #Go through all datasets in time series (forget yt parallel programming)
        for ds_idx,ds in enumerate(ts):
            
            #Add fields to dataset

            def __accept_entropy(field,data):
                mw = data.ds.mu
                K = data["kT"]/( data["density"]/(mw*yt.units.mh))**(2./3.)
                return K.in_units("keV*cm**2")

            ds.add_field("ACCEPT_entropy", 
                    function=__accept_entropy,
                    sampling_type="local",
                    units='keV*cm**2')

            def _cold_mass_temp_x(field, data,temp_thresh):
                temp = data['temperature']
                mass = data['density'] * data['cell_volume']
                ind = np.where(temp > temp_thresh)
                mass[ind] = 0.0
                return mass

            cold_mass_temp_threshs = (1e4,1e5,4e7,np.inf)
            cold_mass_full_names = [ ("gas", f"cold_mass_temp_{temp_thresh:.1e}") 
                                    for temp_thresh in cold_mass_temp_threshs ]


            for temp_thresh,full_name in zip(cold_mass_temp_threshs,cold_mass_full_names):
                function=lambda field,data,temp_thresh=temp_thresh: (
                    _cold_mass_temp_x(field,data,temp_thresh))
                function.__name__ = full_name[1] #Hack for yt

                ds.add_field(full_name, 
                    function= function,
                    sampling_type="local",
                    units='Msun')
            
            for sds in slice_data_specs:
                if ds_idx >= len_saved[hash(sds)]:
                    print(f"Reading slice {hash(sds)} from AthenaPK file {sim_dir}/{ds}")
                    sys.stdout.flush()

                    slc = ds.slice(axis=sds.axis,coord=sds.coord,center=sds.center)
                    frb = slc.to_frb(width=sds.width,resolution=sds.resolution)

                    for field in sds.fields:
                        slice_datas[hash(sds)][field][ds_idx] = frb[field].copy()
                        slice_maxs[hash(sds)][field][ds_idx] = frb[field].max()
                        slice_mins[hash(sds)][field][ds_idx] = frb[field].min()

                    #It's unnecessary but easier to store 
                    #for axis in (sds.axis_1,sds.axis_2):
                    #    field_datas[hash(sds)][axis][ds_idx] = frb[axis].copy() 
        for sds in slice_data_specs:
            #Save the data
            sds.save_slice_ts(slice_datas[hash(sds)],slice_maxs[hash(sds)],slice_mins[hash(sds)],data_dir)
    return slice_datas,slice_mins,slice_maxs
