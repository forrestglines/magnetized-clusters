################################################################################
# Makes all_field_stats.hdf5, an HDF5 file written by yt (with units) with
# statistics (mins, maxs, means, etc.)  about all relevant fields from a single
# simulation.
# 
# Arguments
#   sim_dir : Directory of the simulation
################################################################################

import sys
from pathlib import Path

from mpi4py import MPI
comm = MPI.COMM_WORLD
my_rank = comm.Get_rank()
root_rank = 0

if len(sys.argv) < 2:
    if (my_rank == root_rank):
        print("Insufficient arguments. Use case:\n"
              "      sim_dir : Directory of the simulation")
    raise Exception("Invalid commandline arguments")

sim_dir = sys.argv[1]

if (my_rank == root_rank):
    print(f"Reading statistics from {sim_dir}")

import field_stat_specs as fss

all_field_stats = fss.read_all_field_stats(sim_dir)
fss.save_all_field_stats(all_field_stats,sim_dir)


