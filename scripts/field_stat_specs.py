################################################################################
# Defines the FieldStatSpec, which specifies fields and statistics (mins, maxs,
# means, etc.) to read from a simulations. Also provides useful functions for
# reading, saving to disk, and loading said statistic s
################################################################################

import numpy as np
import itertools
import os

# IMPORTANT: MAKE SURE TO USE THE `parthenon-frontend` BRANCH FROM 
# https://github.com/forrestglines/yt/tree/parthenon-frontend

import yt
yt.enable_parallelism()
yt.set_log_level(50)

from yt import derived_field
@derived_field(name="ACCEPT_entropy",units="keV*cm**2",sampling_type="cell")
def __accept_entropy(field,data):
    mw = data.ds.mu
    K = data["kT"]/( data["density"]/(mw*yt.units.mh))**(2./3.)
    return K.in_units("keV*cm**2")

from mpi4py import MPI
comm = MPI.COMM_WORLD
my_rank = comm.Get_rank()
root_rank = 0

#sim_dir = "/scratch/cvz/glines/magnetized-clusters/cluster_testing/feedback_suite/large.all_mechs"
cooling_table_filename = "/u/glines/code/magnetized-clusters/inputs/cooling_tables/schure.cooling"

# ## Define and set specifications for statistics
# 
# Defines a "statistic" to read over time from a simulation, including what
# field to examine, what aspects (min, max, total, median, etc.) to read from
# the field, how to treat nans, how to weigh means, how to mask data

class FieldStatsSpec:
    """
    StatisticSpec
    
    Defines a "statistic" to read over time from a simulation,
    including what field to examine, what aspects (min, max, total, median, etc.)
    to read from the field, how to treat nans, how to weigh means, how to mask data
    
    """
    def __init__(self,
                 field, #Field to integrate
                 statistics, #Which statistics (total, min, max, mean)
                 stats_label=None,
                 derived_field_name=None,
                 weighting_func=None, #Function accepting (field,data) returning weighted field
                 masking_func=None, #Function accepting (field,data) returning masked field
                 yt_field_units=None,
                 nan_policy="ignore",
                 mean_weighting_field=None,
                 ):
        
        self.field = field
        self.statistics = tuple(statistics)

        
        self.need_derived_field = ( weighting_func != None
            or masking_func != None
            or nan_policy != "ignore")
        
        self.weighting_func = weighting_func
        self.masking_func = masking_func
        
        if self.need_derived_field and derived_field_name == None:
            if stats_label == None:
                my_hash = hash((self.field,self.statistics,weighting_func,masking_func))
                derived_field_name = f"fss_{field}_{my_hash}"
            else:
                derived_field_name = f"fss_{stats_label}"
                
        self.yt_field_units = yt_field_units
        if self.need_derived_field and self.yt_field_units == None:
            raise Exception("Need to provide yt_field_units if derived_field is needed")

            
        #Name given to yt derived field used to generate data
        self.derived_field_name=derived_field_name
        
        if self.need_derived_field:
            self.yt_field_name = self.derived_field_name
        else:
            self.yt_field_name = field
                
        if stats_label == None:
            if derived_field_name == None:
                stats_label =  field
            else:
                stats_label = derived_field_name
        self.stats_label = stats_label
        
        self.nan_policy = nan_policy
        
        self.mean_weighting_field = mean_weighting_field
        
    def add_derived_fields_to_ds(self,ds):
        if self.need_derived_field:
            def derived_field_func(field,data,fss=self):
                out = data[fss.field]
                if fss.weighting_func != None:
                    out = fss.weighting_func(out,data)
                if fss.masking_func != None:
                    out = fss.masking_func(out,data)
                    
                if fss.nan_policy == "zero":
                    out[np.isnan(out)] = 0
                elif fss.nan_policy == "mask":
                    out = out[ np.logical_not(np.isnan(out)) ]
                    
                return out
                
            derived_field_func.__name__ = self.derived_field_name

            ds.add_field(("gas",self.derived_field_name), 
                function= derived_field_func,
                sampling_type="local",units=self.yt_field_units)


# cell_volume weighting funciton: Useful for integrating cell-averaged quantities
def cell_volume_weighting_func(field_to_weight,data):
    return field_to_weight*data["cell_volume"]

# cold_temp masking function: Best for computing the total mass of "cold" gas,
# where the "cold" temperature threshold can be specified
def cold_temp_masking_func(field_to_mask,data,temp_thresh):
    mask = np.where(data["temperature"].in_units("K")>temp_thresh.in_units("K"))
    field_to_mask[mask ]=0
    return field_to_mask
        
cold_mass_thresh_strs=["1e4","4e4","1e5","4e7"]

# Create a list of statistics to read
field_stats_specs = [
    #Here starts the list for unmasked quantities
    FieldStatsSpec("density",["mean","min","max"],mean_weighting_field="cell_volume"),
    FieldStatsSpec("pressure",["mean","min","max"],mean_weighting_field="cell_mass"),
    FieldStatsSpec("ACCEPT_entropy",["mean","min","max"],mean_weighting_field="cell_mass"),
    FieldStatsSpec("temperature",["mean","min","max"],mean_weighting_field="cell_mass"),
    FieldStatsSpec("cooling_rate",["mean","min","max"],mean_weighting_field="cell_mass"),
    FieldStatsSpec("cooling_time",["mean","min","max"],mean_weighting_field="cell_mass"),
    FieldStatsSpec("cooling_rate",["total"],stats_label="cell_cooling_rate",
                   weighting_func=cell_volume_weighting_func,yt_field_units="erg/s"),
    FieldStatsSpec("cooling_rate",["total"],stats_label="cell_cooling_rate",
               weighting_func=cell_volume_weighting_func,yt_field_units="erg/s"),
] + [
    #Here starts the list for cold temperature masked mass statistics,
    #one for each cold temperature threshold
    FieldStatsSpec("density",["total"],stats_label=f"cell_cold_mass_t<={temp_thresh}K",
               weighting_func=cell_volume_weighting_func,
               masking_func= lambda field_to_mask,data,temp_thresh=temp_thresh:
                   cold_temp_masking_func(field_to_mask,data,
                   temp_thresh=yt.YTQuantity(float(temp_thresh),"K")),
               yt_field_units="code_mass")
    for temp_thresh in cold_mass_thresh_strs
]


#Convert that list into a dictionary

#How to index
#field_stats_specs[fss.stats_label]

field_stats_specs = { fss.stats_label: fss for fss in field_stats_specs}

# ## Operations for reading/saving/loading Stats Data
# 
# Functions to read, save, and load the statistics defined above from a single simulation directory

def read_all_field_stats(sim_dir):
    """
    Read statistics from the primitive outputs in sim_dir and return them in an all_field_stats object
    """
    outputs_glob = f"{sim_dir}/parthenon.restart.*.rhdf"
    
    #Create a time series to read from 
    ts = yt.load(outputs_glob,parameters={
        "cooling_table_filename":cooling_table_filename,
        "cooling_table_log_temp_col":0,
        "cooling_table_log_lambda_col":1,
        "cooling_table_lambda_units_cgs":1})

    times = yt.YTArray([ds.current_time for ds in ts]).in_units("Myr")

    # Define an empty storage dictionary for collecting information
    # in parallel through processing
    storage = {}

    #Process in parallel the datasets from the globbed time series
    #(is this feasible for large datasets? to hold all slices on one processor?)
    for sto, ds in ts.piter(storage=storage):

        ds_field_stats = {stats_label:{} for stats_label in field_stats_specs.keys()}

        reg = ds.all_data()

        for fss in field_stats_specs.values():
            fss.add_derived_fields_to_ds(ds)
            for stat in fss.statistics:
                if stat == "min":
                    ds_field_stats[fss.stats_label]["min"]=reg[fss.yt_field_name].min()
                elif stat == "max":
                    ds_field_stats[fss.stats_label]["max"]=reg[fss.yt_field_name].max()
                elif stat == "mean":
                    ds_field_stats[fss.stats_label]["mean"]=reg.quantities.weighted_average_quantity(
                        fss.yt_field_name,fss.mean_weighting_field)
                elif stat == "total":
                    ds_field_stats[fss.stats_label]["total"]=reg.quantities.total_quantity(fss.yt_field_name)

        sto.result = {"ds_field_stats":ds_field_stats}
        sto.result_id = str(ds)
    
    storage_items = sorted(storage.items())
    
    #all_field_stats[stats_label][stat][output_idx]
    all_field_stats = {stats_label:{ 
            stat:yt.YTArray([ value["ds_field_stats"][stats_label][stat] for key,value in storage_items ])
        for stat in fss.statistics } for stats_label,fss in field_stats_specs.items()}
    
    all_field_stats["times"] = times
    
    return all_field_stats
def save_all_field_stats(all_field_stats,save_dir):
    if my_rank == root_rank:
        fake_ds = {"current_time": yt.YTQuantity(0, "Myr")} # To appease cosmology units dependent on time in yt
        
        save_name = f"{save_dir}/all_field_stats.h5"
        
        my_data = { f"{stats_label}:{stat}":all_field_stats[stats_label][stat].in_base("cgs")
                   for stats_label,fss in field_stats_specs.items() 
                   for stat in fss.statistics }
        
        my_data["times"] = all_field_stats["times"].in_base("cgs")
        
        yt.save_as_dataset(fake_ds,save_name,my_data)

def load_all_field_stats(filename):
    ds = yt.load(filename)
    
    all_field_stats  = { stats_label:{ stat: ds.data[f"{stats_label}:{stat}"] 
                                      for stat in fss.statistics } 
                        for stats_label,fss in field_stats_specs.items()}
    all_field_stats["times"] = ds.data["times"]
    return all_field_stats

def HACK_truncate_last_output(in_stats):
    out_stats  = { stats_label:{ stat: in_stats[stats_label][stat][:-1]
                                      for stat in fss.statistics } 
                        for stats_label,fss in field_stats_specs.items()}
    out_stats["times"] = out_stats["times"][:-1]
    return out_stats



