################################################################################
# Makes all_field_stats.hdf5, an HDF5 file written by yt (with units) with
# statistics (mins, maxs, means, etc.)  about all relevant fields from a single
# simulation.
# 
# Arguments
#   sim_dir : Directory of the simulation
################################################################################

import sys
from pathlib import Path

if len(sys.argv) < 2:
    print("""Insufficient arguments. Use case:
                sim_dir : Directory of the simulation
          """)
    raise Exception("Invalid commandline arguments")

sim_dir = sys.argv[1]


print(f"Reading statistics from {sim_dir}")

import numpy as np
import itertools
import os

# IMPORTANT: MAKE SURE TO USE THE `parthenon-frontend` BRANCH FROM 
# https://github.com/forrestglines/yt/tree/parthenon-frontend

import yt
yt.enable_parallelism()

yt.set_log_level(50)

import field_stats_specs as fss


all_field_stats = read_all_field_stats(sim_dir)
save_all_field_stats(all_field_stats,sim_dir)


for sim_name,sim_dir in zip(sim_names,sim_dirs):    
    if not os.path.exists(f"{sim_dir}/all_field_stats.h5"):
        if not os.path.exists(f"{sim_dir}/parthenon.restart.00000.rhdf"):
            print(f"Skipping stats for {sim_name}, simulation data not present or not yet run")
        else:
            print(f"Reading stats for {sim_name}")


# ## Functions to make history plots/figures

# In[ ]:


def plot_fig_suite(fig,axes,sim_names,sim_dirs,sim_labels,add_line_func):
    """
    Plots add_line_func for each simulation on different axis in a figure
    """
    
    for sim_name,sim_dir,sim_label,ax in zip(sim_names,sim_dirs,sim_labels,axes.flatten()):
        
        if not os.path.exists(f"{sim_dir}/all_field_stats.h5"):
            print(f"Skipping plotting {sim_name}")
            continue
        
        print("Plotting")
        all_field_stats  = load_all_field_stats(f"{sim_dir}/all_field_stats.h5")

        add_line_func(fig,ax,all_field_stats)
        
        ax.annotate(text=sim_label,xy=(0.05,0.95),
                    ha="left",va="top",
                    xycoords="axes fraction",fontsize=SMALL_SIZE)
        
def plot_ax_suite(fig,ax,sim_names,sim_dirs,sim_labels,add_line_func):
    """
    Plots add_line_func for each simulation on one axis
    """
    
    for sim_name,sim_dir,sim_label in zip(sim_names,sim_dirs,sim_labels):
        
        if not os.path.exists(f"{sim_dir}/all_field_stats.h5"):
            print(f"Skipping plotting {sim_name}")
            continue
            
        all_field_stats  = load_all_field_stats(f"{sim_dir}/all_field_stats.h5")

        add_line_func(fig,ax,all_field_stats,label=sim_label)


# ## Make a cold mass (with different cold temperature thresholds) over time, one simulation to an axis

# In[ ]:


def add_cold_masses_to_ax(fig,ax,all_field_stats):
    """
    Adds the cold mass vs time to an axis from the
    all_field_stats object for each of the cold mass thresholds
    """
    cold_mass_stats_labels = [ stats_labels for stats_labels in all_field_stats.keys() 
                               if "cell_cold_mass" in stats_labels]

    cold_mass_thresh_strs=["1e4","4e4","1e5","4e7"]

    linestyle = itertools.cycle((':', '-.', '--', '-')) 

    for thresh_str in cold_mass_thresh_strs:
        stat_label = f"cell_cold_mass_t<={thresh_str}K"

        ax.plot(all_field_stats["times"].in_units("Myr"),
                all_field_stats[stat_label]["total"].in_units("msun"),
                linestyle = next(linestyle),
                label=f"T<= {thresh_str}K") 
        
fig,axes = plt.subplots(nrows=4,ncols=4,figsize=(8,10),sharex="all",sharey="all")
        

plot_fig_suite(fig,axes,sim_names,sim_dirs,sim_labels,add_cold_masses_to_ax)

axes[0,0].legend()
axes[0,0].set_yscale("log")


fig.tight_layout(rect=[0.007,0.005,1.0,1.0])

fig.text(0.5, 0.0005, "Time [Myr]", ha='center', va='bottom')
fig.text(0.0005, 0.5, "Total cold mass [$M_\\odot$]", ha='left', va='center',rotation=90)

#fig.text(0.0005, 0.5, 'Energy [E]', ha='left', va='center', rotation='vertical')

#plt.savefig(f"{fig_dir}/cold_masses.pdf",bbox_inches="tight",padding=0)


# ### Plot minimum temperature, minimum cooling time, total cold mass under 1e5K,  over time on three axes for all simulations

# In[ ]:



def add_minimum_temperature_to_ax(fig,ax,all_field_stats,label):
"""
Adds the minimum temperature vs time to an axis
from the all_field_stats object
"""
ax.plot(all_field_stats["times"].in_units("Myr"),
        all_field_stats["temperature"]["min"].in_units("K"),
        label=label)

def add_minimum_cooling_time_to_ax(fig,ax,all_field_stats,label):
"""
Adds the minimum cooling time vs time to an axis
from the all_field_stats object
"""
ax.plot(all_field_stats["times"].in_units("Myr"),
        all_field_stats["cooling_time"]["min"].in_units("Myr"),
        label=label)

def add_cold_mass_1e5K_to_ax(fig,ax,all_field_stats,label):
"""
Adds the mass of gas under 1e5 K vs time to an axis
from the all_field_stats object
"""
ax.plot(all_field_stats["times"].in_units("Myr"),
        all_field_stats[f"cell_cold_mass_t<=1e5K"]["total"].in_units("msun"),
        label=label) 

    
#fig,axes = plt.subplots(nrows=4,ncols=4,figsize=(8,10),sharex="all",sharey="all")
    
fig,axes = plt.subplots(nrows=1,ncols=3,figsize=(7,3),sharex="all",sharey="none")

ax = axes[0]
plot_ax_suite(fig,ax,sim_names,sim_dirs,sim_labels,add_minimum_temperature_to_ax)
ax.set_ylabel("Minimum Cooling Time [Myr]")

ax = axes[1]
plot_ax_suite(fig,ax,sim_names,sim_dirs,sim_labels,add_minimum_cooling_time_to_ax)
ax.set_ylabel("Temperature [K]")
ax.set_yscale("log")

ax = axes[2]
plot_ax_suite(fig,ax,sim_names,sim_dirs,sim_labels,add_cold_mass_1e5K_to_ax)
ax.set_ylabel("Total cold mass (<1e5K) [$M_\\odot$]")
ax.set_yscale("log")

axes[0].legend()

fig.tight_layout(rect=[0.000,0.005,1.0,1.0])

fig.text(0.5, 0.0005, "Time [Myr]", ha='center', va='bottom')
#fig.text(0.0005, 0.5, "Minimum Cooling Time [Myr]", ha='left', va='center',rotation=90)

#fig.text(0.0005, 0.5, 'Energy [E]', ha='left', va='center', rotation='vertical')

#plt.savefig(f"{fig_dir}/cold_masses.pdf",bbox_inches="tight",padding=0)


# In[ ]:





# In[ ]:




