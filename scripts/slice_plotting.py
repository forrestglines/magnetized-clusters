################################################################################
# Defines and makes x,y,z slice plots from a single simulation.
# 
################################################################################

import sys
from pathlib import Path


import os #os.makedirs
import glob 
import numpy as np
import re

import hashlib
import json

# IMPORTANT: MAKE SURE TO USE THE `parthenon-frontend` BRANCH FROM 
# https://github.com/forrestglines/yt/tree/parthenon-frontend

import yt
#Define a cooling table for yt to read from
cooling_table_filename="/u/glines/code/magnetized-clusters/inputs/cooling_tables/schure.cooling"

import slice_data
from slice_data import default_slice_data_specs

import matplotlib as mpl
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import ImageGrid
import numpy as np

# Make figure backgrounds white instead of transparent
mpl.rcParams["figure.facecolor"]="white"

# ## Define slice plot parameters
# 
# Define how to plot each field from the slices we've read. This includes what kind of color-axes to use (i.e. linear, log, symlog), colormap, whether to include quiver/vector arrows with two other fields, and a label.

class SlicePlotSpec:
 """
 SliceDataSpec
 
 Define how to plot each field from the slices we've read. 
 This includes what kind of color-axes to use (i.e. linear, log, symlog), 
 colormap, whether to include quiver/vector arrows with two other fields, 
 and a label.
 """
 def __init__(self,
             slice_data_spec,
             field,
             units,
             norm_type = "linear",
             norm_params = None,
             norm_sym = False,
             norm = None,
             cmap = "viridis",
             quiver_fields=None,
             quiver_params=None,
             field_label=None):
     self.slice_data_spec = slice_data_spec
     self.field = field
     self.units = units
     self.norm_type = norm_type
     if norm_params is None:
         norm_params = {}
     self.norm_params = norm_params
     self.norm_sym = norm_sym
     self.norm = norm
     
     self.quiver_fields = quiver_fields
     self.quiver_params = quiver_params
     
     if norm_type == "log" and norm_sym == True:
         raise Exception('Logarithmic norm (norm_type="log") and symmetric norm_sym (norm_sym=True) are incompatible')
         
     if field_label is None:
         field_label = f"{field} in {units}"
         
     self.field_label = field_label
     

# Create a dictionary for how to plot each field

#How to index the dictionary:
#slice_plot_specs[hash(sds)][field]

default_slice_plot_specs = { hash(sds): { sps.field:sps for sps in [
     SlicePlotSpec(sds,"density","g/cm**3","log",cmap="viridis",field_label="$\\rho$ [$\\text{g}/\\text{cm}{}^3$]"),#norm=mpl.colors.LogNorm(yt.YTQuantity(e-24,1e-26,"g/cm**3"))),
     SlicePlotSpec(sds,"pressure","dyne/cm**2","log",cmap="cividis",field_label="$P$ [$\\text{dyne}/\\text{cm}^3$]"),
     SlicePlotSpec(sds,"temperature","K","log",field_label="$T$ [K]"),
     SlicePlotSpec(sds,"cooling_time","Myr","log",cmap="plasma",field_label="$t_{cool}$ [Myr]"),
     SlicePlotSpec(sds,"cooling_rate","erg/s/cm**3","log",cmap="magma",field_label="$\\Lambda_{cool}$ [erg/s/cm${}^3$]"),
     SlicePlotSpec(sds,"velocity_x","cm/s","symlog",norm_sym=True,cmap="PuOr",field_label="$v_x$ [cm/s]"),
     SlicePlotSpec(sds,"velocity_y","cm/s","symlog",norm_sym=True,cmap="PuOr",field_label="$v_y$ [cm/s]"),
     SlicePlotSpec(sds,"velocity_z","cm/s","symlog",norm_sym=True,cmap="PuOr",field_label="$v_z$ [cm/s]"),
     SlicePlotSpec(sds,"mach_number","","log",field_label="$M_s$"),
     SlicePlotSpec(sds,"magnetic_field_x","Gauss","symlog",norm_sym=True,cmap="PiYG",field_label="$B_x$ [Gauss]"),
     SlicePlotSpec(sds,"magnetic_field_y","Gauss","symlog",norm_sym=True,cmap="PiYG",field_label="$B_y$ [Gauss]"),
     SlicePlotSpec(sds,"magnetic_field_z","Gauss","symlog",norm_sym=True,cmap="PiYG",field_label="$B_z$ [Gauss]"),
     SlicePlotSpec(sds,"magnetic_energy_density","erg/cm**3","log",field_label="$E_B$ [erg/cm${}^3$]"),
     SlicePlotSpec(sds,"ACCEPT_entropy","keV*cm**2","log",field_label="$K$ [keV cm${}^2$]"),
 ]} for sds in default_slice_data_specs }


# ## Set up the bounds for the color-axes
# 
# Find `vmin` and `vmax` for each plotted field in each slice from `slice_mins` and `slice_maxs` depending on the color-axis type for each field in each slice

def populate_sps_norms(slice_plot_specs,slice_data_specs,
    slice_mins,slice_maxs):
    # First, finish setting up the coloring norms
    for sds in slice_data_specs:
        for field,sps in slice_plot_specs[hash(sds)].items():

            #Only setup the norm if it's not yet specified
            if sps.norm is None:
                if "vmax" not in sps.norm_params or sps.norm_params["vmax"] is None:
                    sps.norm_params["vmax"] = slice_maxs[hash(sds)][field].max().in_units(sps.units)
                if "vmin" not in sps.norm_params or sps.norm_params["vmin"] is None:
                    sps.norm_params["vmin"] = slice_mins[hash(sds)][field].min().in_units(sps.units)

                if sps.norm_sym:
                    vmax = np.abs(yt.YTArray([sps.norm_params["vmin"],sps.norm_params["vmax"]])).max()

                    sps.norm_params["vmax"] =  vmax
                    sps.norm_params["vmin"] = -vmax

                if sps.norm_type == "linear":
                    sps.norm = mpl.colors.Normalize(**sps.norm_params)

                if sps.norm_type == "log":
                    if sps.norm_params["vmin"] <= 0:
                        sps.norm_params["vmin"] = 1e-10*sps.norm_params["vmax"]
                    sps.norm = mpl.colors.LogNorm(**sps.norm_params)

                elif sps.norm_type == "symlog":
                    if "linthresh" not in sps.norm_params or sps.norm_params["linthresh"] is None:
                        sps.norm_params["linthresh"] = 1e-4*sps.norm_params["vmax"]
                    sps.norm = mpl.colors.SymLogNorm(**sps.norm_params)

            #Since everything is by reference, is this last line necessary?
            #slice_plot_specs[i] = sps
            slice_plot_specs[hash(sds)][field] = sps


# ## Define figures
# 
# Define the figures to be plotted and saved, including plots to make, number of rows and columns, figure size, title if any, and prefix to the figure filename

class SliceFigureSpec:
    """
    SliceFigureSpec
    
    Define the figures to be plotted and saved,
    including plots to make, number of rows and
    columns, figure size, title if any, and prefix
    to the figure filename
    """
    def __init__(self,
                slice_plot_specs,
                nrows,
                ncols,
                save_prefix,
                title=None,
                figsize=None,
                tweak_func=None,
                ):
        #self.slice_data_specs[field]
        self.slice_plot_specs = slice_plot_specs
        self.nrows = nrows
        self.ncols = ncols
        self.save_prefix = save_prefix
        self.title = title
        if figsize is None:
            figsize = (0.5 + 2.5*ncols,0.5 + 2*nrows)
        self.figsize = figsize

        self.tweak_func=tweak_func
        #self.slice_plot_specs[]
        
# Make a list of figures to make movies of

default_slice_figure_specs = [
    #Here starts figures for each slice orientation
    SliceFigureSpec(
        slice_plot_specs= [
            default_slice_plot_specs[hash(sds)][field] 
            for field in [
                "density","temperature",
                "cooling_time","ACCEPT_entropy",
                "mach_number","magnetic_energy_density"]],
        nrows=3,
        ncols=2,
        save_prefix=f"slice_{sds.axis}",
        title=f"{sds.axis}-Slice"
        )
    for sds in default_slice_data_specs ]


def make_slice_figures(
    fig_dir,sim_dir,
    slice_figure_specs,slice_plot_specs,slice_data_specs,
    output_idxs_to_plot=None):

    outputs_glob = f"{sim_dir}/parthenon.restart.*.rhdf"
    output_filenames = glob.glob(outputs_glob)
    output_filenames.sort()

    ts = yt.DatasetSeries(output_filenames,parameters={
        "cooling_table_filename":cooling_table_filename,
        "cooling_table_log_temp_col":0,
        "cooling_table_log_lambda_col":1,
        "cooling_table_lambda_units_cgs":1})

    if output_idxs_to_plot == None:
        output_idxs_to_plot = range(len(output_filenames))

    slice_datas,slice_mins,slice_maxs = slice_data.get_slice_datas(
        sim_dir,fig_dir,slice_data_specs)

    populate_sps_norms(slice_plot_specs,slice_data_specs,
        slice_mins,slice_maxs)

    #Make sure figure directory exists
    os.makedirs(fig_dir, exist_ok=True)  

    # ## Plot figures for each output
    # 
    # Plots the figures defined above into pngs. You can collate these pngs together into a `.mov` with FFmpeg, such as with
    # 
    # ```
    # module load FFmpeg
    # ffmpeg -framerate 10 -pattern_type glob -i "slice_x*.png" -f mp4 -vcodec h264 -pix_fmt yuv420p -b:v 8M -r 10 slice_x.mov -y && ffmpeg -framerate 10 -pattern_type glob -i "slice_y*.png" -f mp4 -vcodec h264 -pix_fmt yuv420p -b:v 8M -r 10 slice_y.mov -y  && ffmpeg -framerate 10 -pattern_type glob -i "slice_z*.png" -f mp4 -vcodec h264 -pix_fmt yuv420p -b:v 8M -r 10 slice_z.mov -y
    # ```
    # 
    # or into a `.gif` with Imagemagick `convert` using
    # 
    # ```
    # convert -delay 20 -loop 0 slice_x_*.png slice_x.gif && convert -delay 20 -loop 0 slice_y_*.png slice_y.gif  && convert -delay 20 -loop 0 slice_z_*.png slice_z.gif
    # ```

    for output_idx in output_idxs_to_plot:
        if( output_idx >= len(output_filenames) ):
            print(f"Skipping {output_idx}, data doesn't exist")
            continue
        for sfs in slice_figure_specs:

            #FIXME Is sharexy="all" correct? 
            # For now, only if all plots on a figure share the same slice resolution
            fig,axes= plt.subplots(nrows=sfs.nrows,ncols=sfs.ncols,
                                   sharex="all",sharey="all",
                                   squeeze=False,
                                   figsize=sfs.figsize)

            for ax,sps in zip(axes.flatten(),sfs.slice_plot_specs):
                
                #Grab slice_data_spec
                sds = sps.slice_data_spec
                
                #Grab the slice_data
                ax_slice_data = slice_datas[hash(sds)][sps.field][output_idx].in_units(sps.units)
                
                #Grab the coordinate data #HACK assuming kpc axes
                X,Y = sds.get_positions()

                pcm = ax.pcolormesh(X.in_units("kpc"),Y.in_units("kpc"),
                    ax_slice_data,norm=sps.norm)

                fig.colorbar(pcm, ax=ax,label=sps.field_label)
                
            #HACK assuming shared axes on all figures in the plot
            for i in range(sfs.nrows):
                axes[i,0].set_ylabel(f"{sds.axis_2_name} [kpc]")
            for j in range(sfs.ncols):
                axes[-1,j].set_xlabel(f"{sds.axis_1_name} [kpc]")

            fig.suptitle(f"{sfs.title} {ts[output_idx].current_time.in_units('Myr'):.1f}" )

            if( sfs.tweak_func != None ):
                sfs.tweak_func(fig,axes,ts[output_idx])
            else:
                fig.tight_layout()

            plt.savefig(f"{fig_dir}/{sfs.save_prefix}_{output_idx:05d}.png",dpi=300)
            plt.clf()
            plt.close(fig)


