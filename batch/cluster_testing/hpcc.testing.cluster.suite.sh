#!/bin/bash --login
########## SBATCH Lines for Resource Request ##########
 
#SBATCH --time=03:50:00             # limit of wall clock time - how long the job will run (same as -t)
#SBATCH --nodes=1                   # number of different nodes - could be an exact number or a range of nodes (same as -N)
#SBATCH --ntasks=40                 # number of tasks - how many tasks (nodes) that you require (same as -n)
#SBATCH --cpus-per-task=1           # number of CPUs (or cores) per task (same as -c)
#SBATCH --gres=gpu:a100:4
#SBATCH --job-name=feedback_suite    # you can give your job a name for easier identification (same as -J)
#SBATCH -o out/feedback_suite
 
########## Command Lines for Job Running ##########

date
source $HOME/code/athenaPK/env_scripts/cuda-RelWithDebInfo-ampere80.sh

athenapk_bin=$HOME/code/athenaPK/builds/cuda-RelWithDebInfo-ampere80/bin/athenaPK
cooling_file=$HOME/code/athenaPK/athenaPK/inputs/cluster/schure.cooling

ngpus=$(expr $SLURM_NNODES \* 4)

#Create a list of all simulations
mesh_sizes=("tiny" "medium" "large")
feedback_mechs=("thermal" "magnetic" "kinetic")

#Generate list of simulation names
sim_names=()
for mesh_size in ${mesh_sizes[@]}; do
    sim_names+=("${mesh_size}.all_mechs")
    for feedback_mech in ${feedback_mechs[@]}; do
        sim_names+=("${mesh_size}.${feedback_mech}" "${mesh_size}.no_${feedback_mech}")
    done
done

for sim_name in ${sim_names[@]}; do
    athinput=$HOME/code/magnetized-clusters/inputs/cluster_testing/feedback_suite/feedback.${sim_name}.input
    WORKDIR=/mnt/gs18/scratch/users/glinesfo/magnetized-clusters/cluster_testing/feedback_suite/${sim_name}
    mkdir -p $WORKDIR
    cd $WORKDIR
    echo "Checking on ${sim_name} in `pwd`"
    if test -f "RUN_FINISHED"; then
        echo "Skipping ${sim_sim}, already finished"
    elif test -f "cluster.00000.rst"; then
        lastfile=`echo *.rst | sort | grep -oE '[^ ]+$'`
        echo "Restarting ${sim_sim}"
        cmd="srun -n ${ngpus} ${athenapk_bin} -r $lastfile"
        echo $cmd
        eval " $cmd"
        touch RUN_FINISHED
    else
        echo "Starting ${sim_sim}"
        cmd="srun -n ${ngpus} ${athenapk_bin} -i  ${athinput}"
        echo $cmd
        eval " $cmd"
        touch RUN_FINISHED
    fi    
done

echo "FINISHED"
date
