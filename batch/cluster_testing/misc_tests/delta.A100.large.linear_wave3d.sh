#!/bin/bash --login
########## SBATCH Lines for Resource Request ##########

#SBATCH --mem-per-gpu=60g
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=4
#SBATCH --cpus-per-task=4    # <- match to OMP_NUM_THREADS
#SBATCH --partition=gpuA100x4      # <- or one of: gpuA100x4 gpuA40x4 gpuA100x8 gpuMI100x8
#SBATCH --account=cvz-delta-gpu
#SBATCH --job-name=large.linear_wave3d
#SBATCH --time=08:00:00      # hh:mm:ss for the job
#SBATCH --constraint="scratch"
#SBATCH -o out/large.linear_wave3d-%j.out
### GPU options ###
#SBATCH --gpus-per-node=4
#SBATCH --gpu-bind=closest     # <- or closest


########## Command Lines for Job Running ##########

date
SCRATCH=/scratch/cvz/glines/


source $HOME/code/athenapk-project/env_scripts/cuda-RelWithDebInfo-ampere80.sh

athenapk_exe=$HOME/code/athenapk-project/builds/cuda-RelWithDebInfo-ampere80/bin/athenaPK

test_dir=$SCRATCH/magnetized-clusters/cluster_testing/misc_tests/large.linear_wave3d
input_file=$HOME/code/magnetized-clusters/inputs/cluster_testing/misc_tests/large.linear_wave3d.input
cooling_file=$HOME/code/athenapk-project/athenapk/inputs/cooling_tables/schure.cooling

mkdir -p $test_dir
cd $test_dir

pwd

cmd="srun -n 4 ${athenapk_exe} -i ${input_file} cooling/table_filename=${cooling_file}"   ### call your executable. (use srun instead of mpirun.)

echo $cmd
eval "$cmd"

scontrol show job $SLURM_JOB_ID     ### write job information to SLURM output file.


echo "JOB DONE"
date
