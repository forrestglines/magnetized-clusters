#!/bin/bash --login
########## SBATCH Lines for Resource Request ##########

#SBATCH --mem-per-gpu=60g
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=4
#SBATCH --cpus-per-task=16    # <- match to OMP_NUM_THREADS
#SBATCH --partition=gpuA100x4      # <- or one of: gpuA100x4 gpuA100x4 gpuA100x8 gpuMI40x8
#SBATCH --account=cvz-delta-gpu
#SBATCH --job-name=myjobtest
#SBATCH --time=00:30:00      # hh:mm:ss for the job
#SBATCH --constraint="scratch"
#SBATCH -o out/tiny_A100-%j.out
### GPU options ###
#SBATCH --gpus-per-node=4
#SBATCH --gpu-bind=closest     # <- or closest

 
########## Command Lines for Job Running ##########

date
SCRATCH=/scratch/cvz/glines/

num_gpus=4

source $HOME/code/athenapk-project/env_scripts/cuda-RelWithDebInfo-ampere80.sh 

athenapk_bin=$HOME/code/athenapk-project/builds/cuda-RelWithDebInfo-ampere80/bin/athenaPK

testing_root=$SCRATCH/magnetized-clusters/
inputs_dir=$HOME/code/magnetized-clusters/inputs/
athenapk_exe=$HOME/code/athenapk-project/builds/cuda-RelWithDebInfo-ampere80/bin/athenaPK


cooling_file=$inputs_dir/schure.cooling

test_name=cluster_testing/misc_tests/tiny_test

test_dir=${testing_root}/${test_name}_A100
input_file=${inputs_dir}/${test_name}.input


cp ${cooling_file} ${test_dir}
#cmd="srun -n ${num_gpus} ${athenapk_exe} -i ${input_file} cooling/table_filename=${cooling_file}"   ### call your executable. (use srun instead of mpirun.)
cmd="srun -n ${num_gpus} ${athenapk_exe} -i ${input_file}"   ### call your executable. (use srun instead of mpirun.)

mkdir -p $test_dir
cd $test_dir

pwd

echo $cmd
eval "$cmd"
 
scontrol show job $SLURM_JOB_ID     ### write job information to SLURM output file.


echo "JOB DONE"
date
