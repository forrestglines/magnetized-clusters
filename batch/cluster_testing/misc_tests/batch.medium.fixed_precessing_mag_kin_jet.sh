#!/bin/bash --login
########## SBATCH Lines for Resource Request ##########
 
#SBATCH --time=16:00:00             # limit of wall clock time - how long the job will run (same as -t)
#SBATCH --nodes=1                   # number of different nodes - could be an exact number or a range of nodes (same as -N)
#SBATCH --ntasks=4                 # number of tasks - how many tasks (nodes) that you require (same as -n)
#SBATCH --cpus-per-task=1           # number of CPUs (or cores) per task (same as -c)
#SBATCH --gres=gpu:a100:4
#SBATCH --job-name=med_fixed    # you can give your job a name for easier identification (same as -J)
#SBATCH -o out/med_fixed
 
########## Command Lines for Job Running ##########

date

source $HOME/code/athenaPK/env_scripts/cuda-RelWithDebInfo-ampere80.sh 

athenapk_bin=$HOME/code/athenaPK/builds/cuda-RelWithDebInfo-ampere80/bin/athenaPK

testing_root=/mnt/gs18/scratch/users/glinesfo/magnetized-clusters/
inputs_dir=$HOME/code/magnetized-clusters/inputs/
athenapk_exe=$HOME/code/athenaPK/builds/cuda-RelWithDebInfo-ampere80/bin/athenaPK


cooling_file=$inputs_dir/schure.cooling

test_name=cluster_testing/misc_tests/medium.fixed_precessing_mag_kin_jet

test_dir=${testing_root}/${test_name}
input_file=${inputs_dir}/${test_name}.input

#cmd="srun -n 4 ${athenapk_exe} -i ${input_file} cooling/table_filename=${cooling_file}"   ### call your executable. (use srun instead of mpirun.)
cmd="srun -n 4 ${athenapk_exe} -i ${input_file}"   ### call your executable. (use srun instead of mpirun.)

mkdir -p $test_dir
cd $test_dir

pwd

echo $cmd
eval "$cmd"
 
scontrol show job $SLURM_JOB_ID     ### write job information to SLURM output file.


echo "JOB DONE"
date
