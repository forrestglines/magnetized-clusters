#!/bin/bash --login
########## SBATCH Lines for Resource Request ##########

#SBATCH --mem=240g
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=64
#SBATCH --cpus-per-task=1    # <- match to OMP_NUM_THREADS
#SBATCH --partition=cpu      #
#SBATCH --account=cvz-delta-cpu
#SBATCH --job-name=make_all_field_stats
#SBATCH --time=8:00:00      # hh:mm:ss for the job
#SBATCH --constraint="scratch"
#SBATCH --exclusive
#SBATCH -o out/make_all_field_stats-%j.out


########## Command Lines for Job Running ##########

date
if [ ! -z "$SLURM_JOB_ID" ]
then
    scontrol show job $SLURM_JOB_ID     ### write job information to SLURM output file.
fi
SCRATCH=/scratch/cvz/glines/

make_stats_script=$HOME/code/magnetized-clusters/scripts/make_all_field_stats.py
sim_root=$SCRATCH/magnetized-clusters/cluster_testing/feedback_suite

source $HOME/code/athenapk-project/env_scripts/cuda-RelWithDebInfo-ampere80.sh

function make_all_field_stats () {
    local sim_dir=$1

    if [ -d $sim_dir ]  && [ ! -f ${sim_dir}/all_field_stats.h5 ]
    then
        echo "Working on $sim_dir"
        srun -n 64 python $make_stats_script $sim_dir

    else
        if [ ! -d $sim_dir ]
        then
            echo "Skipping $sim_dir, sim data does not exist"
        elif [ -f ${sim_dir}/all_field_stats.h5 ]
        then
            echo "Skipping $sim_dir, all_field_stats already exists"
        fi
    fi

}



sim_names=("small.dipole.all_mechs")
#sim_names=("small.euler.hydro" "small.glmmhd.hydro" "small.glmmhd.uniform_b_field")

for sim_name in ${sim_names[@]}; do

    sim_dir=${sim_root}/${sim_name}
    make_all_field_stats ${sim_dir}

    wait
done


echo "JOB DONE"
date
