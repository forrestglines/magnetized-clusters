#!/bin/bash --login
########## SBATCH Lines for Resource Request ##########

#SBATCH --mem-per-gpu=60g
#SBATCH --nodes=4
#SBATCH --ntasks-per-node=4
#SBATCH --cpus-per-task=16    # <- match to OMP_NUM_THREADS
#SBATCH --partition=gpuA100x4      # <- or one of: gpuA100x4 gpuA40x4 gpuA100x8 gpuMI100x8
#SBATCH --account=cvz-delta-gpu
#SBATCH --job-name=no_snia.small.magnetic
#SBATCH --time=48:00:00      # hh:mm:ss for the job
#SBATCH --constraint="scratch"
#SBATCH --exclusive
#SBATCH -o out/no_snia.small.magnetic-%j.out
### GPU options ###
#SBATCH --gpus-per-node=4
#SBATCH --gpu-bind=none     # <- or closest


########## Command Lines for Job Running ##########

date
SCRATCH=/scratch/cvz/glines/


source $HOME/code/athenapk-project/env_scripts/cuda-RelWithDebInfo-ampere80.sh

athenapk_exe=$HOME/code/athenapk-project/builds/cuda-RelWithDebInfo-ampere80/bin/athenaPK

test_dir=$SCRATCH/magnetized-clusters/cluster_testing.no_snia/feedback_suite/small.magnetic
input_file=$HOME/code/magnetized-clusters/inputs/cluster_testing/feedback_suite/no_snia.small.magnetic.input
cooling_file=$HOME/code/magnetized-clusters/inputs/cooling_tables/schure.cooling

mkdir -p $test_dir
cd $test_dir

pwd



if ! compgen -G "${test_dir}/parthenon.restart.*.rhdf" > /dev/null; then
    #Start simulation from beginning
    cmd="srun -n 16 ${athenapk_exe} -i ${input_file} cooling/table_filename=${cooling_file}"
    echo $cmd
    eval "$cmd"
elif [ -f  "${test_dir}/parthenon.restart.final.rhdf" ]; then
    #final restart exists, start from there
    cmd="srun -n 16 ${athenapk_exe} -r ${test_dir}/parthenon.restart.final.rhdf"
    echo $cmd
    eval "$cmd"
else
    #Only numbered outputs were made, try the last one
    last_rhdf=`ls ${test_dir}/parthenon.restart.*.rhdf | tail -n 1`
    cmd="srun -n 16 ${athenapk_exe} -r ${last_rhdf}"
    echo $cmd
    eval "$cmd"
fi



scontrol show job $SLURM_JOB_ID     ### write job information to SLURM output file.


echo "JOB DONE"
date
