#!/bin/bash --login
########## SBATCH Lines for Resource Request ##########

#SBATCH --mem=240g
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=64
#SBATCH --cpus-per-task=1    # <- match to OMP_NUM_THREADS
#SBATCH --partition=cpu      #
#SBATCH --account=cvz-delta-cpu
#SBATCH --job-name=make_all_field_stats
#SBATCH --time=48:00:00      # hh:mm:ss for the job
#SBATCH --constraint="scratch"
#SBATCH --exclusive
#SBATCH -o out/make_all_field_stats-%j.out


########## Command Lines for Job Running ##########

date
if [ ! -z "$SLURM_JOB_ID" ]
then
    scontrol show job $SLURM_JOB_ID     ### write job information to SLURM output file.
fi
SCRATCH=/scratch/cvz/glines/

make_stats_script=$HOME/code/magnetized-clusters/scripts/make_all_field_stats.py
sim_root=$SCRATCH/magnetized-clusters/cluster_testing/feedback_suite

source $HOME/code/athenapk-project/env_scripts/cuda-RelWithDebInfo-ampere80.sh

function make_all_field_stats () {
    local sim_dir=$1

    if [ -d $sim_dir ]  && [ ! -f ${sim_dir}/all_field_stats.h5 ]
    then
        echo "Working on $sim_dir"
        srun -n 64 python $make_stats_script $sim_dir

    else
        if [ ! -d $sim_dir ]
        then
            echo "Skipping $sim_dir, sim data does not exist"
        elif [ -f ${sim_dir}/all_field_stats.h5 ]
        then
            echo "Skipping $sim_dir, all_field_stats already exists"
        fi
    fi

}



#sim_sizes=("medium" "large" "huge")
sim_sizes=("tiny" "small" "medium" "large" "huge")
mechs=("magnetic" "thermal" "kinetic")

#for sim_size in ${sim_sizes[@]}; do
#
#    #All mechs f
#    sim_dir=${sim_root}/${sim_size}.all_mechs
#    make_all_field_stats ${sim_dir}
#
#    for mech in ${mechs[@]}; do
#        #Singular mech simulations
#        sim_dir=${sim_root}/${sim_size}.${mech}
#        make_all_field_stats ${sim_dir}
#
#        #Dual mech simulations
#        sim_dir=${sim_root}/${sim_size}.no_${mech}
#        make_all_field_stats ${sim_dir}
#    done
#
#    wait
#done
for sim_size in ${sim_sizes[@]}; do

    #All mechs f
    sim_dir=${sim_root}/${sim_size}.noB0.all_mechs
    make_all_field_stats ${sim_dir}

    for mech in ${mechs[@]}; do
        #Singular mech simulations
        sim_dir=${sim_root}/${sim_size}.noB0.${mech}
        make_all_field_stats ${sim_dir}

        #Dual mech simulations
        sim_dir=${sim_root}/${sim_size}.noB0.no_${mech}
        make_all_field_stats ${sim_dir}
    done

    wait
done


echo "JOB DONE"
date
