#!/bin/bash --login
########## SBATCH Lines for Resource Request ##########

#SBATCH --mem=240g
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=64
#SBATCH --cpus-per-task=1    # <- match to OMP_NUM_THREADS
#SBATCH --partition=cpu      #
#SBATCH --account=cvz-delta-cpu
#SBATCH --job-name=no_snia.make_movies
#SBATCH --time=48:00:00      # hh:mm:ss for the job
#SBATCH --constraint="scratch"
#SBATCH --exclusive
#SBATCH -o out/make_movies-%j.out


########## Command Lines for Job Running ##########

date
scontrol show job $SLURM_JOB_ID     ### write job information to SLURM output file.
SCRATCH=/scratch/cvz/glines/

movies_script=$HOME/code/magnetized-clusters/scripts/make_slice_movies.py
sim_root=$SCRATCH/magnetized-clusters/cluster_testing.no_snia/feedback_suite

source $HOME/code/athenapk-project/env_scripts/cuda-RelWithDebInfo-ampere80.sh
module load ImageMagick

function make_movies () {
    local sim_dir=$1
    local sim_title=$2

    if [ -d $sim_dir ] 
    then
        echo "Working on $sim_title"
        python $movies_script "$sim_dir" "$sim_title"
        fig_dir=${sim_dir}/figures

        convert -delay 20 -loop 0 ${fig_dir}/slice_x_*.png ${fig_dir}/slice_x.gif && convert -delay 20 -loop 0 ${fig_dir}/slice_y_*.png ${fig_dir}/slice_y.gif  && convert -delay 20 -loop 0 ${fig_dir}/slice_z_*.png ${fig_dir}/slice_z.gif

    else
        echo "Skipping $sim_title, sim_dir does not exists."
    fi

}



sim_sizes=("tiny" "small" "medium" "large" "huge")
mechs=("magnetic" "thermal" "kinetic")

for sim_size in ${sim_sizes[@]}; do

    #All mechs f
    sim_dir=${sim_root}/${sim_size}.all_mechs
    sim_title="${sim_size^} All Mechs"
    make_movies ${sim_dir} "${sim_title}" &

    for mech in ${mechs[@]}; do
        #Singular mech simulations
        sim_dir=${sim_root}/${sim_size}.${mech}
        sim_title="${sim_size^} ${mech^}"
        make_movies ${sim_dir} "${sim_title}" &

        #Dual mech simulations
        sim_dir=${sim_root}/${sim_size}.no_${mech}
        sim_title="${sim_size^} No ${mech^}"
        make_movies ${sim_dir} "${sim_title}" &
    done

    wait
done


echo "JOB DONE"
date
