#!/bin/bash --login
########## SBATCH Lines for Resource Request ##########
 
#SBATCH --time=02:00:00             # limit of wall clock time - how long the job will run (same as -t)
#SBATCH --nodes=1                   # number of different nodes - could be an exact number or a range of nodes (same as -N)
#SBATCH --ntasks=1                  # number of tasks - how many tasks (nodes) that you require (same as -n)
#SBATCH --gres=gpu:v100:1
#SBATCH --mem=10G
#SBATCH --job-name just_cooling      # you can give your job a name for easier identification (same as -J)
 
########## Command Lines for Job Running ##########

date

source $HOME/code/athenaPK/env_scripts/cuda-RelWithDebInfo-volta70.sh 

testing_root=/mnt/gs18/scratch/users/glinesfo/magnetized-clusters/testing
inputs_dir=$HOME/code/magnetized-clusters/inputs/athenaPK/testing
athenapk_exe=$HOME/code/athenaPK/builds/cuda-RelWithDebInfo-volta70/bin/athenaPK


cooling_file=$inputs_dir/schure.cooling

test_name=thermal_testing/just_cooling

test_dir=${testing_root}/${test_name}
input_file=${inputs_dir}/${test_name}.input

cmd="srun -n 1 ${athenapk_exe} -i ${input_file} cooling/table_filename=${cooling_file}"   ### call your executable. (use srun instead of mpirun.)

mkdir -p $test_dir
cd $test_dir

pwd

echo $cmd
eval "$cmd"
 
scontrol show job $SLURM_JOB_ID     ### write job information to SLURM output file.


echo "JOB DONE"
date
