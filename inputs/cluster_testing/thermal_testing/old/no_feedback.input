
<comment>
problem   = Isolated galaxy cluster

<job>
problem_id = cluster   # problem ID: basename of output filenames

<parthenon/output1>
file_type  = hst       # History data dump
dt         = 1e-3      # time increment between outputs (1 Myr)

<parthenon/output2>
file_type  = hdf5      # HDF5 data dump
variables  = prim      # Variables to be output
dt         = 5.e-3     # Time increment between outputs (10 Myr)
id         = prim      # Name to append to output
hdf5_compression_level = 0

<parthenon/output3>
file_type  = rst      # restart data dump
dt         = 5.e-2     # Time increment between outputs (10 Myr)
hdf5_compression_level = 0

<parthenon/time>
cfl        = 0.3        # The Courant, Friedrichs, & Lewy (CFL) Number
nlim       = -1         # cycle limit
tlim       = 10       # time limit (10 Gyr)
integrator  = vl2       # time integration algorithm
perf_cycle_offset  = 10 # interval for stdout summary info


<parthenon/mesh>
refinement  = static
nghost = 2

nx1        = 64       # Number of zones in X1-direction
x1min      =-1.6       # minimum value of X1
x1max      = 1.6       # maximum value of X1
ix1_bc     = outflow   # inner-X1 boundary flag
ox1_bc     = outflow   # outer-X1 boundary flag

nx2        = 64       # Number of zones in X2-direction
x2min      =-1.6       # minimum value of X2
x2max      = 1.6       # maximum value of X2
ix2_bc     = outflow   # inner-X2 boundary flag
ox2_bc     = outflow   # outer-X2 boundary flag

nx3        = 64       # Number of zones in X3-direction
x3min      =-1.6       # minimum value of X3
x3max      = 1.6       # maximum value of X3
ix3_bc     = outflow   # inner-X3 boundary flag
ox3_bc     = outflow   # outer-X3 boundary flag

<parthenon/static_refinement0>
x1min = -0.4
x1max =  0.4
x2min = -0.4
x2max =  0.4
x3min = -0.4
x3max =  0.4
level = 3

<parthenon/static_refinement1>
x1min = -0.1
x1max =  0.1
x2min = -0.1
x2max =  0.1
x3min = -0.1
x3max =  0.1
level = 5

<parthenon/static_refinement2>
x1min = -0.0125
x1max =  0.0125
x2min = -0.0125
x2max =  0.0125
x3min = -0.0125
x3max =  0.0125
level = 8


<parthenon/meshblock>
nx1        = 32        # Number of zones in X1-direction
nx2        = 32        # Number of zones in X2-direction
nx3        = 32        # Number of zones in X3-direction

<hydro>
fluid = euler
gamma = 1.6666666666666667 # gamma = C_p/C_v
eos = adiabatic
riemann = hllc
reconstruction = plm
use_scratch = false
scratch_level = 0 # 0 is actual scratch (tiny); 1 is HBM
Tfloor = 10000.0

first_order_flux_correct = True

He_mass_fraction = 0.25

<units>
#Units parameters
code_length_cgs = 3.085677580962325e+24
code_mass_cgs = 1.98841586e+47
code_time_cgs = 3.15576e+16

<cooling>
enable_cooling=tabular
table_filename=schure.cooling
log_temp_col=0
log_lambda_col=1
lambda_units_cgs=1.0

integrator=rk45
cfl=0.1
min_timestep=1.0
max_iter=100
d_e_tol=1e-08
d_log_temp_tol=1e-08

<problem/cluster>
hubble_parameter = 0.0715898515654728

<problem/cluster/gravity>
#Include gravity as a source term
gravity_srcterm = true

#Which gravitational fields to include
include_nfw_g = True
which_bcg_g = HERNQUIST
include_smbh_g = True

#NFW parameters
c_nfw = 6.0
m_nfw_200 = 10.000000000000002

#BCG parameters
m_bcg_s = 0.0010000000000000002
r_bcg_s = 0.004

#SMBH parameters
m_smbh = 1.0000000000000002e-06

#Smooth gravity at origin, for numerical reasons
g_smoothing_radius = 0.0

<problem/cluster/entropy_profile>
#Entropy profile parameters
k_0 = 8.851337676479303e-121
k_100 = 1.3277006514718954e-119
r_k = 0.1
alpha_k = 1.1

<problem/cluster/hydrostatic_equilibrium>
#Fix density at radius to close system of equations
r_fix = 2.0
rho_fix = 0.01477557589278723

#Building the radii at which to sample initial rho,P
r_sampling = 4.0
max_dr = 0.001

<problem/cluster/agn_triggering>
triggering_mode = COLD_GAS
accretion_radius = 0.0005
cold_temp_thresh= 100000.0
cold_t_acc= 0.1
bondi_alpha= 100.0
bondi_beta= 2.0
bondi_n0= 2.9379989445851786e+72

write_to_file = True

<problem/cluster/precessing_jet>
jet_phi= 0.15
jet_theta_dot= 628.3185307179587
jet_theta0= 0.2

<problem/cluster/agn_feedback>
disabled = True
efficiency = 0.001
magnetic_fraction = 0
thermal_fraction = 1
kinetic_fraction = 0

thermal_radius = 0.0005
kinetic_jet_radius  = 0.0005
kinetic_jet_height  = 0.0005


<problem/cluster/magnetic_tower>

alpha = 20
l_scale = 0.001
initial_field = 0.0
l_mass_scale = 0.001
